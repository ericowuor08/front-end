import React from 'react';
import Layout from '../../../components/Layout';
import MyOrderLayout from '../../../components/MyOrderLayout';
const draft = () => {
  return (
    <Layout>
      <MyOrderLayout>
        <div>Draft</div>
      </MyOrderLayout>
    </Layout>
  );
};

export default draft;
